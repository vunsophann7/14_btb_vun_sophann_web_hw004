import React, { Component } from "react";
import TableUserInfo from "./TableUserInfo";
export default class InputForm extends Component {
  constructor() {
    super();
    this.state = {
      userInfo: [],
      userEmail: "",
      userName: "",
      userAge: "",
      switchButton: "Pending",
    };
  }
  // user input area
  getInputValue = (inputValue) => {
    this.setState({
      [inputValue.target.name]: inputValue.target.value,
    });
  };

  // submit button
  submitButton = () => {
    const newUserInfoObject = {
      id: this.state.userInfo.length + 1,
      username: this.state.userName,
      email: this.state.userEmail,
      age: this.state.userAge,
      switchButton: this.state.switchButton,
    };
    this.setState({
      userInfo: [...this.state.userInfo, newUserInfoObject],
    });
  };
  switchButtonClick = (id) => {
    this.state.userInfo.map((data) => {
      if (data.id === id) {
        data.switchButton =
          data.switchButton === "Pending" ? "Done" : "Pending";
      }
      return "";
    });
    this.setState({
      data: this.state.userInfo,
    });
  };

  render() {
    return (
      <div className="w-screen flex justify-center">
        <div className="w-[80%]">
          <h1 className="text-5xl text-center p-5 text">
            <span className="text-[#810CA8]">Please Fill Your </span>Information
          </h1>
          <div className="form-control">
            <label className="label">
              <span className="label-text text-lg">Your Email</span>
            </label>
            <label className="input-group">
              <span>
                <img
                  src="https://cdn-icons-png.flaticon.com/128/732/732200.png"
                  alt=""
                  className="w-7"
                />
              </span>
              <input
                type="text"
                name="userEmail"
                placeholder="info@gmail.com"
                className="input input-bordered input-primary w-full"
                onChange={this.getInputValue}
              />
            </label>
            <label className="label">
              <span className="label-text text-lg">Username</span>
            </label>
            <label className="input-group">
              <span>
                <img
                  src="https://cdn-icons-png.flaticon.com/128/3135/3135715.png"
                  alt=""
                  className="w-7"
                />
              </span>
              <input
                type="text"
                placeholder="username"
                name="userName"
                className="input input-bordered input-primary w-full"
                onChange={this.getInputValue}
              />
            </label>
            <label className="label">
              <span className="label-text text-lg">Age</span>
            </label>
            <label className="input-group">
              <span>
                <img
                  src="https://cdn-icons-png.flaticon.com/128/833/833472.png"
                  alt=""
                  className="w-7"
                />
              </span>
              <input
                type="text"
                placeholder="age"
                name="userAge"
                className="input input-bordered input-primary w-full"
                onChange={this.getInputValue}
              />
            </label>
            <div className="w-full flex justify-center py-5">
              <button
                className=" btn btn-outline btn-primary text-white w-48"
                onClick={this.submitButton}
              >
                Register
              </button>
            </div>
            {/* throw to Table User Information components */}
            <TableUserInfo
              tblData={this.state.userInfo}
              switchButtonClick={this.switchButtonClick}
            />
          </div>
        </div>
      </div>
    );
  }
}
