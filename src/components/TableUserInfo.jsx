import React, { Component } from "react";
import Swal from "sweetalert2";
import "animate.css";
export default class TableUserInfo extends Component { 
  // alert information
  alertInformation = (itemValue) => {
    Swal.fire({
      title:
        "ID : " +
        itemValue.id +
        "\nEmail : " +
        itemValue.email +
        "\nUsername : " +
        itemValue.username +
        "\nAge : " +
        itemValue.age,
      showClass: {
        popup: "animate__animated animate__fadeInDown",
      },
      hideClass: {
        popup: "animate__animated animate__fadeOutUp",
      },
    });
  };
  render() {
    return (
      <div className="w-full flex justify-center">
        <div className="w-full">
          <table className="table-fixed border-collapse w-full">
            <thead>
              <tr className="text-center">
                <th className="w-20 p-4">ID</th>
                <th>Email</th>
                <th>Username</th>
                <th>Age</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody className="text-white p-4 text-center">
              {this.props.tblData.map((tblItem) => (
                <tr
                  className={
                    tblItem.id % 2 !== 0 ? "bg-[#C147E9]" : "bg-[#810CA8]"
                  }
                >
                  <td className="w-4">{tblItem.id}</td>
                  <td>{tblItem.email === "" ? "null" : tblItem.email}</td>
                  <td>{tblItem.username === "" ? "null" : tblItem.username}</td>
                  <td>{tblItem.age === "" ? "null" : tblItem.age}</td>
                  <td className="flex justify-around p-2">
                    <button
                      onClick={() => this.props.switchButtonClick(tblItem.id)}
                      className={`${
                        tblItem.switchButton === "Done"
                          ? `btn btn-success text-white w-1/2  mr-1`
                          : `btn btn-error text-white w-1/2 mr-1 `
                      }`}
                    >
                      {tblItem.switchButton}
                    </button>
                    <button
                      className="btn btn-info text-white  w-1/2 mr-8"
                      onClick={() => {
                        this.alertInformation(tblItem);
                      }}
                    >
                      Show more
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
